<?php
require_once('notelib.php');
$id = specialchars($_GET['id']);
$title = specialchars($_GET['title']);
$desc = specialchars($_GET['desc']);

$action = specialchars($_GET['action']);
$tab = specialchars($_GET['tab']);
$oldTab = specialchars($_GET['old_tab']);

if($action == "create"){
    if ($title == null && $desc == null){
        insertNote("新規メモ","", $tab, $oldTab);
    }else {
        insertNote($title, $desc, $tab, $oldTab);
    }
}else if($action == "delete"){
    deleteNote($id, $tab);
}else if($action == "update"){
    if (!$title && !$desc) {
        $title = '新規メモ';
    }
    updateNote($id,$title,$desc, $tab);
}else if($action == "select"){
    $result = selectNote($id, $tab);
}


header("Content-Type:application/json");
header("Cache-Control:no-cache, no-store");
header("Pragma:no-cache");
if (!empty($result)) {
    print json_encode($result);
}else{
    print json_encode(readNotes($tab, $oldTab));
}
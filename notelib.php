<?php
class Note{
    public $id;
    public $title;
    public $desc;
    public $tab;
    public $old_tab;
}

function executeNonQuery($sql, $tab){
    define('TRASH_NUM', 6);

    if ($tab == TRASH_NUM){
        $db = new PDO('sqlite:DB/trash.sqlite');
    }else{
        $db = new PDO('sqlite:DB/note.sqlite');
    }
    $result = $db->query($sql);

    $db = null;

    return $result;

}

function insertNote($title,$desc,$tab, $oldTab){
    define('TRASH_NUM', 6);

    if ($tab == TRASH_NUM){
        $sql = "INSERT INTO Notes (title,desc,tab, old_tab)VALUES('".$title."','".$desc."',".$tab.",".$oldTab.")";
    }else{
        $sql = "INSERT INTO Notes (title,desc,tab)VALUES('".$title."','".$desc."',".$tab.")";
    }

    executeNonQuery($sql, $tab);
}

function deleteNote($id,$tab){
    $sql = "DELETE FROM Notes WHERE (id = ".$id." AND tab =".$tab.")";
    executeNonQuery($sql, $tab);
}

function updateNote($id,$title,$desc,$tab){
    $sql = "UPDATE Notes SET title='".$title."',desc='".$desc."' WHERE (id = ".$id." AND tab =".$tab.")";
    executeNonQuery($sql, $tab);
}

function selectNote($id,$tab){
    $arr = array();

    $sql = "SELECT * FROM Notes WHERE (id = ".$id." AND tab =".$tab.")";
    $result = executeNonQuery($sql, $tab);

    foreach($result as $row) {
        $note = new Note();
        $note->id = $row['id'];
        $note->title =$row['title'];
        $note->desc = $row['desc'];
        $note->tab = $row['tab'];
        $note->old_tab = $row['old_tab'];
        array_push($arr,$note);
    }
    return $arr;
}

function readNotes($tab, $oldTab){
    define('TRASH_NUM', 6);

    $arr = array();

    if ($tab == TRASH_NUM){
        $db = new PDO('sqlite:DB/trash.sqlite');
    }else{
        $db = new PDO('sqlite:DB/note.sqlite');
    }
    if (!$db) {
        $note = new Note();
        $note->id = 1;
        $note->title ='DBerror';
        $note->desc = '';
        array_push($arr,$note);
    }else {
        foreach($db->query('SELECT * FROM Notes WHERE tab = '.$tab.'') as $row) {
            $note = new Note();
            $note->id = $row['id'];
            $note->title =$row['title'];
            $note->desc = $row['desc'];
            $note->tab = $tab;
            $note->old_tab = $oldTab;
            array_push($arr,$note);
        }
    }
    $db = null;

    return $arr;
}

function specialchars($param){
    return sqlite_escape_string($param);
}
